# README #

This repository collects R scripts that analyze and visual content from wikidata

### What is this repository for? ###

This repository is to collect example R scripts for us all to learn. If you have a R script to share, please fork this repository, add it and submit a pull request.

# Making a spatial map in R on data from Wikidata.
![spatialLeprosy.png](https://bitbucket.org/repo/5kxL7g/images/1023450121-spatialLeprosy.png)

This spatial map is made with [R](https://www.r-project.org
) [1] from data in Wikidata on the infectious [disease leprosy](https://www.wikidata.org/wiki/Q36956  ). The data was extracted through [Wikidata’s SPARQL endpoint](https://query.wikidata.org). Wikidata is a very valuable resource since it collects data on different aspects of that topic. The record for leprosy from example, contains names and known synonyms of the disease as well as translations in many languages. External references and identifiers are also captured as well as disease counts. Typically, to integrate this heterogenous set of data, one would need to extract the data from different api’s. 


Some time ago I stumble upon the blog with the appealing title ["SPARQL with R in less than 5 minutes”](http://www.r-bloggers.com/sparql-with-r-in-less-than-5-minutes/). It was indeed quite strait forward and basically a three step process being:

* Loading the required libraries

```
#!R

library(SPARQL)
library(ggplot2)
library(rworldmap)
```

* Do the SPARQL magic (note: When access Wikidata’s SPARQL endpoint outside a webbrowser (e.g.  R), the web address is a bit more extensive:



```
#!R

wdqs <- "https://query.wikidata.org/bigdata/namespace/wdq/sparql"
query <- "PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX v: <http://www.wikidata.org/prop/statement/>
PREFIX qualifier: <http://www.wikidata.org/prop/qualifier/>
PREFIX statement: <http://www.wikidata.org/prop/statement/>

SELECT DISTINCT ?countryLabel ?ISO3Code ?latlon ?prevalence ?year WHERE {
wd:Q36956 wdt:P699 ?doid ; # P699 Disease ontology ID
p:P1193 ?noc . # P1193 prevalence
?noc qualifier:P17 ?country ;
qualifier:P585 ?year ;
statement:P1193 ?prevalence . # P17 country
?country wdt:P625 ?latlon ;
rdfs:label ?countryLabel ;
wdt:P298 ?ISO3Code ;
wdt:P297 ?ISOCode .
FILTER (lang(?countryLabel) = \"en\")
}"

results <- SPARQL(wdqs, query)
```

* Do the R magic

```
#!R

resultMatrix <- as.matrix(results$results)
View(resultMatrix)
sPDF <- joinCountryData2Map(results$results, joinCode = "ISO3", nameJoinColumn = "ISO3Code")
mapCountryData(sPDF, nameColumnToPlot="prevalence",  oceanCol="lightblue", missingCountryCol="white", 
               mapTitle="Number of cases on Tuberculosis added @SWAT4LS tutorial")
View(getMap())
```

## The SPARQL query that extracted the data$$

```
#!SPARQL

PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX v: <http://www.wikidata.org/prop/statement/>
PREFIX qualifier: <http://www.wikidata.org/prop/qualifier/>
PREFIX statement: <http://www.wikidata.org/prop/statement/>

SELECT DISTINCT ?countryLabel ?ISO3Code ?latlon ?prevalence ?year WHERE {
# wd:Q36956 is the wikidata entry on Leprocy.
# Get the disease identifiers
 wd:Q36956 wdt:P699 ?doid ; # P699 Disease ontology ID
	p:P1193 ?prevalence . # P1193 prevalence

# Get the prevalence 
 ?prevalence qualifier:P17 ?country ;
	qualifier:P585 ?year ;
	statement:P1193 ?prevalence . # P17 country

# Get the latitude and longitude coordinates and Country codes for ISO to link data to a map
 ?country wdt:P625 ?latlon ;
	rdfs:label ?countryLabel ;
	wdt:P298 ?ISO3Code ;
	wdt:P297 ?ISOCode .
FILTER (lang(?countryLabel) = "en")
}
```

The script is available on [Bitbucket](https://bitbucket.org/sulab/wikidatarexamples/src/0519cc83ff09c69f810df3059aa47487384089c1/lepraPrevalence.R?fileviewer=file-view-default).

I am not a R savvy person, the R script in this blog was created with contribution from Google. I am reaching out to more R savvy data users to share their awesome scripts with us.